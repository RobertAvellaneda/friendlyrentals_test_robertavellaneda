﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;

namespace fr_stringcalculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");
            Console.ReadLine();
        }

        public static int Add(string numbers)
        {
            var operators = GetOperators(numbers);
            var operatorsNumbers = Converting(operators);
            Validate(operatorsNumbers);
            return SumOperators(operatorsNumbers);
        }

        private static void Validate(int[] operatorsNumbers)
        {
            if (operatorsNumbers.Any(x => x < 0))
            {
                var negativeValues = operatorsNumbers.Select(x => x < 0);
                throw new ArgumentException("Negative values.", negativeValues.ToString());
            }
        }

        private static int SumOperators(int[] operators)
        {
            var result = 0;
            foreach (var op in operators)
            {
                result += op;
            }
            return result;
        }

        private static string[] GetOperators(string numbers)
        {
            var charSeparators = new[] {',', '\n'};
            if (numbers.StartsWith("//"))
            {
                var delimiter = numbers.Substring(2, numbers.IndexOf('\n') - 2);
                Array.Resize(ref charSeparators, 3);
                charSeparators[2] = Convert.ToChar(delimiter);
                numbers = numbers.Remove(0, numbers.IndexOf('\n') + 1);
            }
            
            var operators = numbers.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            return operators;
        }

        private static int[] Converting(string[] values)
        {
            var result = new int[values.Length];

            for (var i = 0; i < values.Length; i++)
            {
                result[i] = int.Parse(values[i]);
            }
            return result;
        }
    }
}
