﻿using System;
using NUnit.Framework;
using fr_stringcalculator;

namespace fr_stringcalculatorTests
{
    [TestFixture]
    public class fr_stringcalculator_Tests
    {
        [Test]
        public void Add_EmptyString_ReturnsZero()
        {
            // Arrange
            var numbers = string.Empty;

            // Action
            var result = Program.Add(numbers);

            // Assert
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Add_SumOnlyOneNumber_ReturnsSingleValue()
        {
            var numbers = "1";

            var result = Program.Add(numbers);

            Assert.AreEqual(1, result);
        }

        [Test]
        public void Add_SumTwoNumbers_ReturnsSumOk()
        {
            var numbers = "1,2";

            var result = Program.Add(numbers);

            Assert.AreEqual(3, result);
        }

        [Test]
        public void Add_SumSeveralNumbers_ReturnSumOk()
        {
            var numbers = "1,2,3";

            var result = Program.Add(numbers);

            Assert.AreEqual(6, result);
        }

        [Test]
        public void Add_SumSeveralNumbersWithNewLineSeparator_ReturnSumOk()
        {
            var numbers = "1\n2,3";

            var result = Program.Add(numbers);

            Assert.AreEqual(6, result);
        }

        [Test]
        public void Add_AcceptDifferentDelimiters_ReturnSumOk()
        {
            var numbers = "//;\n1;2";

            var result = Program.Add(numbers);

            Assert.AreEqual(3, result);
        }

        [Test]
        public void AddSumNegativeOperators_ThrowsException()
        {
            var numbers = "1,-2,3,-4";

            Assert.Throws<ArgumentException>(() => Program.Add(numbers));
        }
    }
}
